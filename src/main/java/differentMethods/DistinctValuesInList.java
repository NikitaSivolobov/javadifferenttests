package differentMethods;

import java.util.ArrayList;
import java.util.List;

public class DistinctValuesInList {

    public ArrayList<Integer> distinctValuesInList(List<Integer> listNumbers) {
        ArrayList<Integer> distinctValuesInList = new ArrayList<>();
        for(int i = 0; i < listNumbers.size(); i++) {
            if (!distinctValuesInList.contains(listNumbers.get(i))) {
                distinctValuesInList.add(listNumbers.get(i));
            }
        }
        return distinctValuesInList;
    }

    public ArrayList<String> distinctValuesInList(ArrayList<String> listNumbers) {
        ArrayList<String> distinctValuesInList = new ArrayList<>();
        for(int i = 0; i < listNumbers.size(); i++) {
            if (!distinctValuesInList.contains(listNumbers.get(i))) {
                distinctValuesInList.add(listNumbers.get(i));
            }
        }
        return distinctValuesInList;
    }
}
