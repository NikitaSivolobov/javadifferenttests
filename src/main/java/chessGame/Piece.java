package chessGame;

public abstract class Piece {
    private String name;
    private Place place;
    private String id;
    private boolean white;
    private boolean killed;

    public Piece(String name, Place place, String id, boolean white) {
        this.name = name;
        this.place = place;
        this.id = id;
        this.white = white;
    }

    public String getName() {
        return name;
    }

    public Place getPlace() {
        return place;
    }

    public String getId() {
        return id;
    }

    public boolean isWhite() {
        return white;
    }

    public boolean isKilled() {
        return killed;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public void setKilled(boolean killed) {
        this.killed = killed;
    }


    @Override
    public String toString() {
        return "Piece{" +
                "name='" + name + '\'' +
                ", place=" + place +
                ", id='" + id + '\'' +
                ", white=" + white +
                ", killed=" + killed +
                '}';
    }
}
