package chessGame;

public class Bishop extends Piece{

    private static final String name = "Bishop";

    public Bishop(Place place, String id, boolean white) {
        super(name, place, id, white);
    }
}
