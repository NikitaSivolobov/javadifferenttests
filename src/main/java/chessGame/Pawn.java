package chessGame;

public class Pawn extends Piece{
    private static final String name = "Pawn";

    public Pawn(Place place, String id, boolean white) {
        super(name, place, id, white);
    }
}
