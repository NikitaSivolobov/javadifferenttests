package chessGame;

public class Queen extends Piece{

    private static final String name = "Queen";

    public Queen(Place place, String id, boolean white) {
        super(name, place, id, white);
    }
}
