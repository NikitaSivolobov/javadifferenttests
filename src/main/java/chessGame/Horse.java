package chessGame;

public class Horse extends Piece{

    private static final String name = "Horse";

    public Horse(Place place, String id, boolean white) {
        super(name, place, id, white);
    }
}
