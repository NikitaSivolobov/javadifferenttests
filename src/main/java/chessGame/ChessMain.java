package chessGame;
import java.util.ArrayList;
import java.util.HashMap;

public class ChessMain {
    public static void main(String[] args) {

        ChessMain main = new ChessMain();
        ArrayList<Player> players = main.createPlayers();

        for(Player player : players) {
            System.out.println(player);
        }
        HashMap <String, Piece> pieceHashMap = main.createPieces();
        System.out.println(pieceHashMap);
        main.play(players, pieceHashMap);

    }

    public ArrayList<Player> createPlayers() {
        Player whitePlayer = new Player("Walter White", "walter@gmail.com", true, 2000, 20);
        Player blackPlayer = new Player("Black Dog", "dog@gmail.com", false, 2500, 45);
        ArrayList<Player> players = new ArrayList<>();
        players.add(whitePlayer);
        players.add(blackPlayer);
        return players;
    }

    public HashMap<String, Piece> createPieces(){
        King whiteKing = new King(new Place("h", 7), "white_king", true);
        King blackKing = new King(new Place("d", 8), "black_king", false);
        Rook whiteRook01 = new Rook(new Place("a", 7), "white_rook01", true);
        Rook whiteRook02 = new Rook(new Place("g", 7), "white_rook02", true);
        Horse blackHorse01 = new Horse(new Place("d", 6), "black_horse01", false);

        HashMap<String, Piece> pieceHashMap = new HashMap<>();
        pieceHashMap.put(whiteKing.getId(), whiteKing);
        pieceHashMap.put(blackKing.getId(), blackKing);
        pieceHashMap.put(whiteRook01.getId(), whiteRook01);
        pieceHashMap.put(whiteRook02.getId(), whiteRook02);
        pieceHashMap.put(blackHorse01.getId(), blackHorse01);
        return pieceHashMap;
    }

    public void play(ArrayList<Player> players, HashMap<String, Piece> hashMap){
        // move 1
        try {
            players.get(0).movePiece(hashMap.get("black_king"), new Place("a", 8));
        }
        catch (IllegalArgumentException error) {
            System.out.println("The move is incorrect, please try again!");
            //retry functionality
        }

        players.get(1).movePiece(hashMap.get("black_horse01"), new Place("c", 8));

    }

}
