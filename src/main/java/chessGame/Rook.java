package chessGame;

public class Rook extends Piece {

    private static final String name = "Rook";

    public Rook(Place place, String id, boolean white) {
        super(name, place, id, white);
    }
}

