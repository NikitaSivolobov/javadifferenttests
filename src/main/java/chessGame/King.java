package chessGame;

public class King extends Piece {

    private static final String name = "King";

    public King(Place place, String id, boolean white) {
        super(name, place, id, white);
    }
}
