package unitTests.differentMethodsTests;

import differentMethods.DistinctValuesInList;
import io.qameta.allure.Description;
import io.qameta.allure.Owner;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;



@Tag("UNIT")
public class DistinctValuesInListTests {

    @Test
    @Owner("Nikita Sivolobov")
    @Description("проверка метода distinctValuesInList")
    public void arrayNumbersTest(){
        DistinctValuesInList distinctValuesInList = new DistinctValuesInList();
        Integer[] seq = new Integer[] { 3, 2, 1, 1, 0, 4, 5, 2, 0, 9 };
        List<Integer> listNumbers = Arrays.asList(seq);
        ArrayList<Integer> testListExpected = new ArrayList<>();
        Collections.addAll(testListExpected, 3, 2, 1, 0, 4, 5, 9);

        Assertions.assertEquals(testListExpected, distinctValuesInList.distinctValuesInList(listNumbers));
    }

    @Test
    @Owner("Nikita Sivolobov")
    @Description("проверка метода distinctValuesInList из файла txt")
    public void arrayNumbersFromFilesTest(){
        DistinctValuesInList distinctValuesInList = new DistinctValuesInList();
        List<String> lines = null;
        try {
            lines = Files.readAllLines(Paths.get("src/main/java/differentMethods/listNumbers"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        ArrayList<String> listNumbers = new ArrayList<>();
        for(String line : lines) {
            listNumbers.add(line);
        }
        ArrayList<String> testListExpected = new ArrayList<>();
        Collections.addAll(testListExpected, "3", "2", "1", "0", "4", "5", "9", "10", "11", "52");

        Assertions.assertEquals(testListExpected, distinctValuesInList.distinctValuesInList(listNumbers));
    }
}
