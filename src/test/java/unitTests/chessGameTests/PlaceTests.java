package unitTests.chessGameTests;

import chessGame.Place;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

@Tag("UNIT")
public class PlaceTests {

    @Test
    public void validSpotTest(){
        Place place = new Place("a", 1);
        Assertions.assertNotNull(place);
        Assertions.assertEquals("a", place.getX());
        Assertions.assertEquals(1, place.getY());
    }

    @Test
    public void invalidXValueTest() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Place place = new Place("k", 1);
        });
    }

    @Test
    public void invalidYValueTest() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Place place = new Place("a", -10);
        });
    }

    @ParameterizedTest(name = "Iteration #{index} -> Number = {0}")
    @ValueSource(ints = {-1, 0, 100})
    public void invalidYValueParamTest(int y) {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Place place = new Place("a", y);
        });
    }
}
