package unitTests.chessGameTests;

import chessGame.ChessMain;
import chessGame.Piece;
import chessGame.Place;
import chessGame.Player;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.stream.Stream;

@Tag("UNIT")
public class PlayerTests {

    private static ArrayList<Player> players;
    private static HashMap<String, Piece> pieceHashMap;

    @BeforeAll
    public static void setUp() {
        ChessMain main = new ChessMain();
        players = main.createPlayers();
        pieceHashMap = main.createPieces();
    }

    @BeforeEach
    public void beforeEachTest() {
        System.out.println("Running before each");
    }

    @AfterEach
    public void afterEachTest() {
        System.out.println("Running after each");
    }

    @AfterAll
    public static void tearDown(){
        System.out.println("Running after all the tests");
    }

    @Test
    public void validMoveTest(){
        players.get(0).movePiece(pieceHashMap.get("white_king"), new Place("h", 8));

        Assertions.assertEquals(8, pieceHashMap.get("white_king").getPlace().getY());
    }

    @Test
    public void playerCreateSuccessTest(){
        Player whitePlayer = new Player("Walter White", "walter@gmail.com", true, 2000, 20);
        Assertions.assertEquals("Walter White", whitePlayer.getName());
        Assertions.assertEquals("walter@gmail.com", whitePlayer.getEmail());
        Assertions.assertTrue(whitePlayer.isWhite());
        Assertions.assertEquals(2000, whitePlayer.getRank());
        Assertions.assertEquals(20, whitePlayer.getAge());
    }


    @Test
    public void invalidMovePieceColorTest(){
        Assertions.assertThrows(IllegalArgumentException.class, () ->{
            players.get(0).movePiece(pieceHashMap.get("black_king"), new Place("h", 8));
        });
    }

    @Test
    public void invalidMoveBadSpotTest(){
        Assertions.assertThrows(IllegalArgumentException.class, () ->{
            players.get(0).movePiece(pieceHashMap.get("white_king"), new Place("z", 8));
        });
    }

    @ParameterizedTest
    @MethodSource("nameError")
    public void playersCreateIncorrectNameParamTest(String name) {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Player whitePlayer = new Player(name, "walter@gmail.com", true, 2000, 20);
        });
    }

    @ParameterizedTest
    @MethodSource("emailError")
    public void playersCreateIncorrecteMailParamTest(String email) {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Player whitePlayer = new Player("Walter White", email, true, 2000, 20);
        });
    }

    static Stream<String> nameError(){
        return Stream.of("", "     ", null);
    }

    static Stream<String> emailError(){
        return Stream.of("", "     ", null, "waltergmail.com", "com");
    }
}
