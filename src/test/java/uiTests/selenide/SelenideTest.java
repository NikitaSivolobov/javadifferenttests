package uiTests.selenide;

import io.qameta.allure.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import uiTests.selenide.pages.JimDunlopPage;


@Tag("UI")
public class SelenideTest extends BaseTest{

    private final static String BASE_URL = "https://www.jimdunlop.com/";
    private final static String SEARCH_STR = "YNGWIE MALMSTEEN 1.5MM";
    private final static String EXPECTED_WORD = "YNGWIE MALMSTEEN 1.5MM";


    @Test
        @Epic("Тестирование с помощью Selenide")
        @Feature("Тестирование сайта Jim Dunlop")
        @Story("Проверка функционала поиска на сайте")
        @Owner("Nikita Sivolobov")
        @Severity(SeverityLevel.BLOCKER)
    @DisplayName("Тест на проверку возможности найти медиатор YNGWIE MALMSTEEN 1.5MM")
    @Description("Тест на проверку возможности найти медиатор YNGWIE MALMSTEEN 1.5MM")
    public void testDunlopMalmsteenPicks(){
        JimDunlopPage mainPage = new JimDunlopPage(BASE_URL);
        mainPage.searchButton();
        mainPage.search(SEARCH_STR);
        String pick = mainPage.getFromSearchTitles();
        Assertions.assertTrue(pick.contains(EXPECTED_WORD));
    }
}
