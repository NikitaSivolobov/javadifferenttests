package uiTests.selenide.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.openqa.selenium.Keys;

import static com.codeborne.selenide.Selenide.$x;

/**
 * Главная страница jim dunlop продукции
 */
public class JimDunlopPage {
    private final SelenideElement searchButton = $x("//a[@data-search='quickSearch']");
    private final SelenideElement textSearchInput = $x("//input[@class='form-input']");
    private final SelenideElement textSearchMalmsteenPicks =
            $x("//*[@id='product-listing-container']//article[@data-entity-id='1710']");


    public JimDunlopPage(String url){
        Selenide.open(url);
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide().screenshots(true).savePageSource(true));
    }

    /**
     * Возвращает наименование первого продукта из списка
     */
    public String getFromSearchTitles(){
        return textSearchMalmsteenPicks.getAttribute("data-name");
    }

    /**
     * Выполняется поиск на сайте по продуктам jim dunlop и нажимается Enter
     * @param textSearch поисковая строка
     */
    public void search(String textSearch){
        textSearchInput.setValue(textSearch);
        textSearchInput.sendKeys(Keys.ENTER);

    }
    public void searchButton(){
        searchButton.click();
    }


}
