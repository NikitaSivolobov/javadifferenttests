package uiTests.selenium.pageobjectwithsteps.pages;

import org.openqa.selenium.By;
import uiTests.selenium.webdriver.BaseSeleniumForPage;

public class AstonObjectAboutUsPage extends BaseSeleniumForPage {
    private By textAboutUs = By.xpath("//h1[@tag='h1']");


    public String getTextAboutUs() {
        return driver.findElement(textAboutUs).getText();
    }


}
