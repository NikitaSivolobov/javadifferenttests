package uiTests.selenium.pageobjectwithsteps.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import uiTests.selenium.webdriver.BaseSeleniumForPage;

public class AstonObjectPricingPage extends BaseSeleniumForPage {
    private By textCoast = By.xpath("//h1[@tag='h1']");
    private By btnDevPO = By.xpath("//button[text()='Разработка пользовательского ПО']");
    private By btnTeamExpansion = By.xpath("//button[text()='Расширение команды']");
    private By btnDesktop = By.xpath("//div[text()='Desktop']");
    private By btnMVP = By.xpath("//label/div[text()='MVP']");
    private By btnUiUx = By.xpath("//label/div[text()='UI/UX-дизайнер']");
    private By btnFinTech = By.xpath("//div[text()='FinTech']");
    private By btnYear = By.xpath("//div[@class='rc-slider-step']/span[@style='left: 48%;']");
    private By btnCost = By.xpath("//button[text()='Узнать стоимость']");
    private By fieldName = By.xpath("//input[@name='name']");
    private By fieldComment = By.xpath("//textarea[@name='message']");
    private By checkConsent = By.xpath("//span[contains(text(), 'Соглашение о неразглашении')]");
    private By checkAgreement = By.xpath("//div[@class='FormField-module--formField--Tscf+ FormField-module--withoutError--gxl9K FormSubmit-module--checkbox--SPlAU'][2]/label");
    private By btnSubmit = By.xpath("//button[text()='Отправить']");
    private By textRecaptcha = By.xpath("//div[@class='CaptchaInfo-module--captchaInfo--CyOJz']");

    @Step("Заполнение формы расчёта стоимости проекта 'Разработка пользовательского ПО'")
    public void ourCostDevObject(String name, String comment){
        jsFindAndClick(driver.findElement(btnDevPO));
        jsFindAndClick(driver.findElement(btnDesktop));
        driver.findElement(btnMVP).click();
        jsFindAndClick(driver.findElement(btnUiUx));
        driver.findElement(btnFinTech).click();
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(btnYear));
        driver.findElement(btnCost).click();
        driver.findElement(fieldName).sendKeys(name);
        driver.findElement(fieldComment).sendKeys(comment);
        driver.findElement(checkConsent).click();
        driver.findElement(btnSubmit).click();
    }

    @Step("Получение текста на странице деталей проекта")
    public String getTextRecaptchaObject(){
        return driver.findElement(textRecaptcha).getText();
    }
}
