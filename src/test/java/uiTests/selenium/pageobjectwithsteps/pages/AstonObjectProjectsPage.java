package uiTests.selenium.pageobjectwithsteps.pages;

import org.openqa.selenium.By;
import uiTests.selenium.webdriver.BaseSeleniumForPage;

public class AstonObjectProjectsPage extends BaseSeleniumForPage {

    private By ourProjects = By.xpath("//h1[@tag='h1']");
    private By textOurProjects = By.xpath("//button[@class='TitleWith-module--descWrapper--QggPl']//span");
    private By textBtnOk = By.xpath(" //button[text()='Подтверждаю']");


    public String getOurProjects() {
        return driver.findElement(ourProjects).getText();
    }

    public String getTextOurProjects() {
        return driver.findElement(textOurProjects).getText();
    }

//    public void waitProject() {
//        wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(textBtnOk)));
//    }

}
