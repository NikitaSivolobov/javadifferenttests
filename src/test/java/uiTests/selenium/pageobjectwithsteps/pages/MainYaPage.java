package uiTests.selenium.pageobjectwithsteps.pages;

import configproperties.ConfigProvider;
import io.qameta.allure.Step;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import uiTests.selenium.pagefactory.pages.AstonAboutUsPage;
import uiTests.selenium.pagefactory.pages.AstonOurProjectsPage;
import uiTests.selenium.pagefactory.pages.AstonPricingPage;
import uiTests.selenium.webdriver.BaseSeleniumForPage;

public class MainYaPage extends BaseSeleniumForPage {
    @FindBy(xpath = "//body")
    private WebElement pageBody;

    @FindBy(css = "input#text")
    private WebElement searchField;

    @FindBy(xpath = "//div[@data-action='voice']")
    private WebElement searchByVoiceButton;

    @FindBy(xpath = "//div[@aria-label='Закрыть']")
    private WebElement btnCloseYaBrowserDownload;

    public MainYaPage() {
        driver.get(ConfigProvider.URL_YA);
        PageFactory.initElements(driver, this);
    }

    @Step("Закрытие окна скачивания Яндекс браузера")
    public void closeYandexBrowser(){
        btnCloseYaBrowserDownload.click();
    }

    @Step("Переход на страницу нажатием Enter")
    public void pressEnter() {
        searchField.sendKeys(Keys.RETURN);
    }

    @Step("Наводим мышку на Голосовой набор")
    public void moveToVoiceSearchButton() {
        action.moveToElement(searchByVoiceButton).build().perform();
    }

    @Step("Поиск поля поиска на Яндексе")
    public void findSearchField() {
        Assertions.assertTrue(isElementFound(By.cssSelector("input#text"), 3),
                "Поле поиска на главной странице Яндекса отсутствует");
    }

    @Step("Проверка на наличие текста")
    public void assertVoiceSearchTooltipContainsText(String expectedText) {
        Assertions.assertEquals(expectedText,
                pageBody.findElement(By.xpath("//*[contains(text(), '" + expectedText + "')]")).getText(),
                "Tooltip Голосовой поиск отсутствует на странице");
    }
}
