package uiTests.selenium.pageobjectwithsteps.steps;

import uiTests.selenium.pageobjectwithsteps.pages.MainYaPage;

public class MainYaSteps {
    private MainYaPage mainYaPage = new MainYaPage();

    public MainYaSteps closeYaBrowser() {
        mainYaPage.closeYandexBrowser();
        return this;
    }

    public MainYaSteps navigateVoiceSearch() {
        mainYaPage.moveToVoiceSearchButton();
        return this;
    }

    public MainYaSteps verifyVoiceSearchText(String text) {
        mainYaPage.assertVoiceSearchTooltipContainsText(text);
        return this;
    }

    public MainYaSteps verifySearchField() {
        mainYaPage.findSearchField();
        return this;
    }
}
