package uiTests.selenium.pageobjectwithsteps;

import io.qameta.allure.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import uiTests.selenium.pageobjectwithsteps.pages.AstonObjectAboutUsPage;
import uiTests.selenium.pageobjectwithsteps.pages.AstonObjectPricingPage;
import uiTests.selenium.pageobjectwithsteps.pages.AstonObjectProjectsPage;
import uiTests.selenium.pageobjectwithsteps.pages.MainYaPage;
import uiTests.selenium.utils.TestConsts;
import uiTests.selenium.webdriver.BaseSeleniumTest;
import uiTests.selenium.webdriver.TestScreenshot;

import static uiTests.selenium.webdriver.BaseSeleniumForPage.steps;


@Tag("UI")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@ExtendWith(TestScreenshot.class)
public class YandexTest extends BaseSeleniumTest {

    @Test
    @Order(1)
        @Epic("Тестирование с помощью Selenium")
        @Feature("Тестирование Yandex")
        @Story("Проверка главной страницы Яндекса")
        @Owner("Nikita Sivolobov")
        @Severity(SeverityLevel.CRITICAL)
    @Description("Тест на присутствие поля поиска на главной странице Яндекса")
    public void checkVoiceSearch() {
        steps.closeYaBrowser()
                .verifySearchField();
    }
//
//    @Test
//    @Order(2)
//        @Epic("Тестирование с помощью Selenium")
//        @Feature("Тестирование Yandex")
//        @Story("Переходы с главной страницы на другие")
//        @Owner("Nikita Sivolobov")
//        @Severity(SeverityLevel.CRITICAL)
//    @Description("Тест на проверку перехода в проекты")
//    public void checkOurProjectsObject() {
//        MainYaPage mainYaPage = new MainYaPage();
//        mainYaPage.openOurProjectsObjectPage();
//
//        AstonObjectProjectsPage astonObjectProjectsPage = new AstonObjectProjectsPage();
//
//        Assertions.assertEquals("Проекты", astonObjectProjectsPage.getOurProjects());
//        Assertions.assertEquals(TestConsts.TEST_PROJECT_TEXT, astonObjectProjectsPage.getTextOurProjects());
//    }
//
//    @Test
//    @Order(3)
//        @Epic("Тестирование с помощью Selenium")
//        @Feature("Тестирование Yandex")
//        @Story("Переходы с главной страницы на другие")
//        @Owner("Nikita Sivolobov")
//        @Severity(SeverityLevel.CRITICAL)
//    @Description("Тест на проверку перехода в информацию о компании")
//    public void checkAboutUsObject() {
//        MainYaPage mainYaPage = new MainYaPage();
//        mainYaPage.openAboutUsPage();
//
//        Assertions.assertEquals("Приветствуем вас в Aston", new AstonObjectAboutUsPage().getTextAboutUs());
//    }

}
