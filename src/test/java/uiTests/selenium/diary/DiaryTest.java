package uiTests.selenium.diary;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

@Tag("UI")
public class DiaryTest {

//    private WebDriver driver;
    protected ThreadLocal<WebDriver> driver = new ThreadLocal<>();
    private WebDriverWait wait;
    private final static String URL = "https://diary.ru/user/registration";
    private final static String LOGIN_DIARY_BUSY = "testName";
    private final static String LOGIN_DIARY_POSITIVE = "test!";
    private final static String EMAIl_DIARY_POSITIVE = "test@yandex.ru";
    private final static String EXPECTED_TEXT_BUSY_LOGIN = "Этот логин уже занят. Постарайтесь быть более оригинальным :)";


    private final By loginDiary = By.id("signupform-username");
    private final By passwordDiary = By.id("signupform-email");
    private final By chkBoxUserConfirm = By.id("chk_box_user_confirm");
    private final By signupBtn = By.id("signup_btn");
    private final By validationInfoLoginLocator = By.xpath("//div[contains(@class,'field-signupform-username')]" +
            "/p[@class='help-block help-block-error']");


    @BeforeAll
    public static void setupClass() {
        WebDriverManager.edgedriver().setup();
    }

    @BeforeEach
    public void setupTest() {
        EdgeOptions edgeOptions = new EdgeOptions();
        edgeOptions.setHeadless(true);
        edgeOptions.addArguments("window-size=1920,1080");
        driver.set(new EdgeDriver(edgeOptions));
        wait = new WebDriverWait(driver.get(), Duration.ofSeconds(20));
        System.out.println("Start browser for tests...");
        driver.get().manage().window().maximize();
        System.out.println("Open website");
    }

    @AfterEach
    public void tearDown() {
        driver.get().close();
        if (driver != null) {
            System.out.println("======");
            System.out.println("PASSED");
            System.out.println("======");
            driver.get().quit();
        }
    }

    @Test
        @Epic("Тестирование с помощью Selenium")
        @Feature("Тестирование сайта Diary (Дневники)")
        @Story("Проверка успешной регистрации на сайте")
        @Owner("Nikita Sivolobov")
        @Severity(SeverityLevel.BLOCKER)
    @Description("Тест на проверку возможности зарегистрироваться на сайте")
    @Step("Переход на страницу регистрации и заполнение валидных данных")
    public void diaryPositiveRegistrationTest() {
        driver.get().get(URL);

        boolean flag = false;
        driver.get().findElement(loginDiary).sendKeys(LOGIN_DIARY_POSITIVE);
        driver.get().findElement(passwordDiary).sendKeys(EMAIl_DIARY_POSITIVE);
        driver.get().findElement(chkBoxUserConfirm).click();
        driver.get().findElement(signupBtn).click();

        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(validationInfoLoginLocator));
            WebElement text = driver.get().findElement(validationInfoLoginLocator);
            String actualText = text.getText();

            Assertions.assertEquals(EXPECTED_TEXT_BUSY_LOGIN, actualText);
            Assertions.assertTrue(flag);
        } catch (TimeoutException n) {
            flag = true;
            Assertions.assertTrue(flag);
        }
    }

    @Test
        @Epic("Тестирование с помощью Selenium")
        @Feature("Тестирование сайта Diary (Дневники)")
        @Story("Проверка не возможности зарегистрироваться на сайте")
        @Owner("Nikita Sivolobov")
        @Severity(SeverityLevel.BLOCKER)
    @Description("Тест на невозможность регистрации на сайте при вводе уже используемого логина")
    @Step("Переход на страницу регистрации и заполнение не валидных данных")
    public void diaryNegativeLoginRegistrationTest() {
        driver.get().get(URL);

        driver.get().findElement(loginDiary).sendKeys(LOGIN_DIARY_BUSY);
        driver.get().findElement(passwordDiary).sendKeys(EMAIl_DIARY_POSITIVE);
        driver.get().findElement(chkBoxUserConfirm).click();
        driver.get().findElement(signupBtn).click();

        wait.until(ExpectedConditions.elementToBeClickable(validationInfoLoginLocator));
        WebElement text = driver.get().findElement(validationInfoLoginLocator);
        String actualText = text.getText();

        Assertions.assertEquals(EXPECTED_TEXT_BUSY_LOGIN, actualText);
    }

}
