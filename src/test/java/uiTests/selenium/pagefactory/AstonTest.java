package uiTests.selenium.pagefactory;

import io.qameta.allure.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import uiTests.selenium.pagefactory.pages.AstonAboutUsPage;
import uiTests.selenium.pagefactory.pages.AstonPricingPage;
import uiTests.selenium.pagefactory.pages.AstonMainPage;
import uiTests.selenium.utils.TestConsts;
import uiTests.selenium.webdriver.BaseSeleniumTest;
import uiTests.selenium.webdriver.TestScreenshot;

@Tag("UI")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@ExtendWith(TestScreenshot.class)
public class AstonTest extends BaseSeleniumTest {

    @Test
    @Order(1)
        @Epic("Тестирование с помощью Selenium")
        @Feature("Тестирование сайта Астон")
        @Story("Проверка стоимости заказа нового проекта, капча")
        @Owner("Nikita Sivolobov")
        @Severity(SeverityLevel.CRITICAL)
    @DisplayName("Тест на проверку возможности расчёта стоимости нового проекта")
    @Description("Тест на проверку возможности расчёта стоимости нового проекта")
    public void checkCostNewOurProjectShort() {

        AstonMainPage mainPage = new AstonMainPage();
        mainPage.openPricingPage()
                .ourCostDev(TestConsts.TEST_NAME, TestConsts.TEST_COMMENT);

        Assertions.assertEquals(TestConsts.TEST_PRICING_RECAPTCHA, new AstonPricingPage().getTextRecaptcha());
    }

    @Test
    @Order(2)
        @Epic("Тестирование с помощью Selenium")
        @Feature("Тестирование сайта Астон")
        @Story("Проверка стоимости заказа нового проекта, капча")
        @Owner("Nikita Sivolobov")
        @Severity(SeverityLevel.CRITICAL)
    @DisplayName("Тест на проверку возможности расчёта стоимости нового проекта")
    @Description("Тест на проверку возможности расчёта стоимости нового проекта")
    public void checkCostNewOurProjectLong() {

        AstonMainPage mainPage = new AstonMainPage();
        mainPage.openPricingPage()
                        .selectionDevPO()
                .selectionMVP()
                .selectionUiUx()
                        .selectionFinTech()
                                .selectionYear()
                                        .selectionCost()
                                                .sendName(TestConsts.TEST_NAME)
                                                        .sendComment(TestConsts.TEST_COMMENT);

        Assertions.assertEquals(TestConsts.TEST_PRICING_RECAPTCHA, new AstonPricingPage().getTextRecaptcha());
    }

    @Test
    @Order(3)
        @Epic("Тестирование с помощью Selenium")
        @Feature("Тестирование сайта Астон")
        @Story("Переходы с главной страницы на другие")
        @Owner("Nikita Sivolobov")
        @Severity(SeverityLevel.CRITICAL)
    @DisplayName("Тест на проверку перехода в проекты")
    @Description("Тест на проверку перехода в проекты")
    public void checkOurProjects() {
        AstonMainPage mainPage = new AstonMainPage();
        mainPage.openOurProjectsPage()
                .assertTopTextOnOurProjectsPage(TestConsts.TEST_OUR_PROJECT_TEXT, TestConsts.TEST_PROJECT_TEXT);
    }

    @Test
    @Order(4)
        @Epic("Тестирование с помощью Selenium")
        @Feature("Тестирование сайта Астон")
        @Story("Переходы с главной страницы на другие")
        @Owner("Nikita Sivolobov")
        @Severity(SeverityLevel.CRITICAL)
    @DisplayName("Тест на проверку перехода в информацию о компании")
    @Description("Тест на проверку перехода в информацию о компании")
    public void checkAboutUs() {
        AstonMainPage mainPage = new AstonMainPage();
        mainPage.openAboutUsPage();

        Assertions.assertEquals(TestConsts.TEST_ABOUT_US_TEXT, new AstonAboutUsPage().getTextAboutUs());
    }
}
