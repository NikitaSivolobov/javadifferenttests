package uiTests.selenium.pagefactory.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import uiTests.selenium.webdriver.BaseSeleniumForPage;

public class AstonAboutUsPage extends BaseSeleniumForPage {
    @FindBy(xpath = "//h1[@tag='h1']")
    WebElement textAboutUs;

    @FindBy(xpath = "//button[@class='TitleWith-module--descWrapper--QggPl']//span")
    WebElement textOurProjects;


    public AstonAboutUsPage() {
        PageFactory.initElements(driver, this);
    }

    @Step("Получение текста на странице о компании")
    public String getTextAboutUs(){
        return textAboutUs.getText();
    }
}