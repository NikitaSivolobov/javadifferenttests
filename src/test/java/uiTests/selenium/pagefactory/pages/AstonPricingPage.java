package uiTests.selenium.pagefactory.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import uiTests.selenium.webdriver.BaseSeleniumForPage;

public class AstonPricingPage extends BaseSeleniumForPage {
    @FindBy(xpath = "//h1[@tag='h1']")
    WebElement textCoast;

    @FindBy(xpath = "//button[text()='Разработка пользовательского ПО']")
    WebElement btnDevPO;

    @FindBy(xpath = "//button[text()='Расширение команды']")
    WebElement btnTeamExpansion;

    @FindBy(xpath = "//div[text()='Desktop']")
    WebElement btnDesktop;

    @FindBy(xpath = "//label/div[text()='MVP']")
    WebElement btnMVP;

    @FindBy(xpath = "//label/div[text()='UI/UX-дизайнер']")
    WebElement btnUiUx;

    @FindBy(xpath = "//div[text()='FinTech']")
    WebElement btnFinTech;

    @FindBy(xpath = "//div[@class='rc-slider-step']/span[@style='left: 48%;']")
    WebElement btnYear;

    @FindBy(xpath = "//button[text()='Узнать стоимость']")
    WebElement btnCost;

    @FindBy(xpath = "//input[@name='name']")
    WebElement fieldName;

    @FindBy(xpath = "//textarea[@name='message']")
    WebElement fieldComment;

    @FindBy(xpath = "//span[contains(text(), 'Соглашение о неразглашении')]")
    WebElement checkConsent;

    @FindBy(xpath = "//div[@class='FormField-module--formField--Tscf+ FormField-module--withoutError--gxl9K FormSubmit-module--checkbox--SPlAU'][2]/label")
    WebElement checkAgreement;

    @FindBy(xpath = "//button[text()='Отправить']")
    WebElement btnSubmit;

    @FindBy(xpath = "//div[@class='CaptchaInfo-module--captchaInfo--CyOJz']")
    WebElement textRecaptcha;

    public AstonPricingPage() {
        PageFactory.initElements(driver, this);
    }

    @Step("Заполнение формы расчёта стоимости проекта 'Разработка пользовательского ПО'")
    public AstonPricingPage ourCostDev(String name, String comment){
//        jsFindAndClick(btnTeamExpansion);
        jsFindAndClick(btnDevPO);
        jsFindAndClick(btnDesktop);
        btnMVP.click();
        jsFindAndClick(btnUiUx);
        btnFinTech.click();
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", btnYear);
//        btnYear.click();
        btnCost.click();
        fieldName.sendKeys(name);
        fieldComment.sendKeys(comment);
        checkConsent.click();
     //   checkAgreement.click();
        btnSubmit.click();
        return new AstonPricingPage();
    }

    @Step("Выбор РАЗРАБОТКА ПОЛЬЗОВАТЕЛЬСКОГО ПО")
    public AstonPricingPage selectionDevPO(){
        jsFindAndClick(btnDevPO);
        return this;
    }

    @Step("Выбор Desktop")
    public AstonPricingPage selectionDesktop(){
        jsFindAndClick(btnDesktop);
        return new AstonPricingPage();
    }

    @Step("Выбор текущего этапа разработки ПО - MVP")
    public AstonPricingPage selectionMVP(){
        btnMVP.click();
        return new AstonPricingPage();
    }

    @Step("Выбор консультации специалистов UI/UX-дизайнер")
    public AstonPricingPage selectionUiUx(){
        jsFindAndClick(btnUiUx);
        return new AstonPricingPage();
    }

    @Step("Выбор отрасли бизнеса проекта - финтех")
    public AstonPricingPage selectionFinTech(){
        btnFinTech.click();
        return new AstonPricingPage();
    }

    @Step("Выбор продолжительности проекта")
    public AstonPricingPage selectionYear(){
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", btnYear);
        return new AstonPricingPage();
    }

    @Step("Нажатие кнопик Узнать стоимость")
    public AstonPricingPage selectionCost(){
        btnCost.click();
        return new AstonPricingPage();
    }

    @Step("Заполнение формы обратной связи - имя")
    public AstonPricingPage sendName(String name){
        fieldName.sendKeys(name);
        return new AstonPricingPage();
    }

    @Step("Заполнение формы обратной связи - Комментарий")
    public AstonPricingPage sendComment(String comment){
        fieldComment.sendKeys(comment);
        return new AstonPricingPage();
    }

    @Step("Нажатие кнопок в окне отправки обратной связи")
    public AstonPricingPage selectConsent(){
        checkConsent.click();
        btnSubmit.click();
        return new AstonPricingPage();
    }

    @Step("Получение текста на странице деталей проекта")
    public String getTextRecaptcha(){
        return textRecaptcha.getText();
    }
}
