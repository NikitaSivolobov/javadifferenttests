package uiTests.selenium.pagefactory.pages;

import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import uiTests.selenium.webdriver.BaseSeleniumForPage;

public class AstonOurProjectsPage extends BaseSeleniumForPage {
    @FindBy(xpath = "//h1[@tag='h1']")
    WebElement ourProjects;

    @FindBy(xpath = "//button[@class='TitleWith-module--descWrapper--QggPl']//span")
    WebElement textOurProjects;


    public AstonOurProjectsPage() {
        PageFactory.initElements(driver, this);
    }

    public void assertTopTextOnOurProjectsPage(String expectedValue1, String expectedValue2) {
        Assertions.assertEquals(expectedValue1, ourProjects.getText());
        Assertions.assertEquals(expectedValue2, textOurProjects.getText());
    }
}
