package uiTests.selenium.pagefactory.pages;

import configproperties.ConfigProvider;
import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import uiTests.selenium.webdriver.BaseSeleniumForPage;

public class AstonMainPage extends BaseSeleniumForPage {
    @FindBy(xpath = "//body")
    private WebElement pageBody;

    @FindBy(xpath = "//nav[@class='Header-module--navigation--rLyZT']/a")
    private WebElement findOutCost;

    @FindBy(xpath = "//a[text()='Проекты']")
    private WebElement ourProjectsBtn;

    @FindBy(xpath = "//a[text()='Компания']")
    private WebElement aboutUsBtn;

    @FindBy(xpath = "//li[@tabindex='Карьера']")
    private WebElement searchByCareer;

    public AstonMainPage() {
        driver.get(ConfigProvider.URL_ASTON);
        PageFactory.initElements(driver, this);
    }

    @Step("Переход на страницу расчёта стоимости")
    public AstonPricingPage openPricingPage(){
        findOutCost.click();
        return new AstonPricingPage();
    }

    @Step("Переход на страницу Проекты")
    public AstonOurProjectsPage openOurProjectsPage(){
        ourProjectsBtn.click();
        return new AstonOurProjectsPage();
    }

    @Step("Переход на страницу о Компании")
    public AstonAboutUsPage openAboutUsPage(){
        aboutUsBtn.click();
        return new AstonAboutUsPage();
    }
}
