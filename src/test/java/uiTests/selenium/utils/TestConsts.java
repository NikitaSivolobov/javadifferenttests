package uiTests.selenium.utils;

public class TestConsts {
    public static String TEST_NAME = "Test Name";
    public static String TEST_COMMENT = "Тестирование цены специалистов, выбранных для проекта";
    public static String TEST_MAIN_PAGE_TEXT = "ИТ-решения для развития бизнеса";
    public static String TEST_PRICING_TEXT = "Рассчитайте стоимость вашего проекта";
    public static String TEST_PRICING_RECAPTCHA = "Этот сайт защищен reCAPTCHA, к нему применяются Политика конфиденциальности Google и Условия предоставления услуг.";
    public static String TEST_OUR_PROJECT_TEXT = "Проекты";
    public static String TEST_PROJECT_TEXT = "Команда Aston реализовала 320+ успешных проектов для компаний из ведущих отраслей. Изучите наше портфолио, чтобы узнать больше о нашей экспертизе.";
    public static String TEST_ABOUT_US_TEXT = "Приветствуем вас в Aston";
//    public static String TEST_PROJECT_TEXT = "Команда Aston реализовала 320+ успешных проектов для компаний из ведущих отраслей. Изучите наше портфолио, чтобы узнать больше о нашей экспертизе.";
}
