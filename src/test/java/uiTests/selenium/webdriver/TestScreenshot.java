package uiTests.selenium.webdriver;

import io.qameta.allure.Allure;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestWatcher;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Field;
import java.util.Optional;

import static uiTests.selenium.webdriver.BaseSeleniumForPage.driver;

public class TestScreenshot implements TestWatcher {

    @Override
    public void testFailed(ExtensionContext context, Throwable cause) {
        Allure.getLifecycle().addAttachment(
                "screenshot", "image/png", "png"
                , ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)
        );
        driver.close();
        if (driver != null) {
            System.out.println("======");
            System.out.println("TEST FAILED");
            System.out.println("======");
            driver.quit();
        }
    }

    @Override
    public void testSuccessful(ExtensionContext context) {
//        Allure.getLifecycle().addAttachment(
//                "screenshot", "image/png", "png"
//                , ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)
//        );
        driver.close();
        if (driver != null) {
            System.out.println("======");
            System.out.println("TEST PASSED");
            System.out.println("======");
            driver.quit();
        }
    }

//    @Override
//    public void testDisabled(ExtensionContext context, Optional<String> reason) {
//        System.out.println("test disabled");
//        driver.close();
//        if (driver != null) {
//            System.out.println("======");
//            System.out.println("TEST disabled");
//            System.out.println("======");
//            driver.quit();
//        }
//    }
//
//
//    @Override
//    public void testAborted(ExtensionContext context, Throwable cause) {
//        System.out.println("test aborted");
//        driver.close();
//        if (driver != null) {
//            System.out.println("======");
//            System.out.println("TEST aborted");
//            System.out.println("======");
//            driver.quit();
//        }
//    }
}
