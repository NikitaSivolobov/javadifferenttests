package uiTests.selenium.webdriver;

import configproperties.ConfigProvider;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

abstract public class BaseSeleniumTest {
    protected ThreadLocal<WebDriver> driver = new ThreadLocal<>();
//    protected WebDriver driver;

//    @BeforeAll
//    public static void setupClass() {
//        WebDriverManager.edgedriver().setup();
//    }

    @BeforeEach
    public void setupTest() {
        WebDriverManager.edgedriver().setup();
        EdgeOptions edgeOptions = new EdgeOptions();
        edgeOptions.setHeadless(false);
        edgeOptions.addArguments("window-size=1920,1080");
        driver.set(new EdgeDriver(edgeOptions));
        System.out.println("Start browser for tests...");
        driver.get().manage().window().maximize();
//        driver.get().manage().timeouts().pageLoadTimeout(Duration.ofSeconds(10));
        driver.get().manage().timeouts().implicitlyWait(Duration.ofSeconds(4));
        BaseSeleniumForPage.setDriver(driver.get());
        System.out.println("Open website");
    }

    @AfterEach
    public void tearDown() {
        driver.get().close();
        if (driver.get() != null) {
            System.out.println("======");
            System.out.println("BROWSER CLOSED");
            System.out.println("======");
            driver.get().quit();
        }
    }

//    @AfterAll
//    public void tearDownAll() {
//        driver.get().close();
//        if (driver.get() != null) {
//            System.out.println("======");
//            System.out.println("After All");
//            System.out.println("===^===");
//            driver.get().quit();
//        }
//    }
}
