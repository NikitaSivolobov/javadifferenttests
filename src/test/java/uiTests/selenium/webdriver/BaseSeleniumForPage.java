package uiTests.selenium.webdriver;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import uiTests.selenium.pageobjectwithsteps.steps.MainYaSteps;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.time.Duration;
import java.util.List;

abstract public class BaseSeleniumForPage {
    private static final Duration DEFAULT_TIMEOUT_SECONDS = Duration.ofSeconds(10);
    protected static WebDriver driver;
    protected static WebDriverWait wait;
    protected static Actions action;
    protected static JavascriptExecutor executor;
    public static MainYaSteps steps;


    public static void setDriver(WebDriver webDriver) {
        driver = webDriver;
        wait = new WebDriverWait(driver, DEFAULT_TIMEOUT_SECONDS);
        action = new Actions(driver);
        executor = (JavascriptExecutor) driver;
        steps = new MainYaSteps();
    }

    protected void waitAndClick(WebElement locator) {
        new Actions(driver).click(wait.until(ExpectedConditions.elementToBeClickable(locator)));
//        wait.until(ExpectedConditions.elementToBeClickable(locator)).click();
    }

    protected boolean isElementFound(By by, int timeout) {
        List<WebElement> elements = driver.findElements(by);
        for (int i = 0; (i < timeout) && (elements.size() == 0); i++) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            elements = driver.findElements(by);
        }
        return elements.size() > 0;
    }

    protected WebElement waitVisibility(WebElement locator) {
        return wait.until(ExpectedConditions.visibilityOf(locator));
    }

    protected void jsFindAndClick(WebElement locator) {
        executor.executeScript("arguments[0].scrollIntoView(true);", locator);
        locator.click();
    }

    protected void clickWithJavascript(WebElement element) {
        executor.executeScript("arguments[0].click()", element);
    }

    protected void showJS() {
        executor.executeScript("jQuery('#div.SubMenu-module--subMenu--uEulx SubMenu-module--right--LVKIk').show()");
    }

    protected void pasteTextToElementFromClipboard(WebElement element, String text) {
        //copy text to memory buffer
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Clipboard clipboard = toolkit.getSystemClipboard();
        StringSelection strSelection = new StringSelection(text);
        clipboard.setContents(strSelection, null);
        //paste it to the field
        element.sendKeys(Keys.CONTROL, "v");
    }
}
