package uiTests.selenium.avtovokzal;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;


@Tag("UI")
public class FindMoscowTest extends DataAvtovokzal{

    private final By inputCityFrom = By.id("search_from");
    private final By inputCityTo = By.id("search_to");
    private final By dateFrom = By.xpath("//*[@id='search_date1']");
    private final By btnFind = By.xpath("//input[@id='ss02']");
    private final By btnNext = By.xpath("//*[@id='dd2']/div/button");
    private final By getErrorText = By.xpath("//div[@class='row']/span/h1");
    private final By cityVolgograd = By.xpath("//div[@data-id='10']");
    private final By cityMoskow = By.xpath("//div[@data-id='50470870']");

    private WebDriver driver;
    private WebDriverWait wait;
    private JavascriptExecutor executor;


    @BeforeEach
    public void setUp() {
        //Headless режим
//        ChromeOptions option=new ChromeOptions();
//        option.addArguments("headless");

        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
//        driver = new ChromeDriver(option);
        wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        executor = (JavascriptExecutor) driver;
        System.out.println("Start chrome browser for tests..");
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(65));
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(7));

    }

    @AfterEach
    public void cleanUp() {
        if(driver != null) {
            System.out.println("======");
            System.out.println("PASSED");
            System.out.println("======");
            driver.quit();
        }
    }

    @Test
    public void findMoscow()  {

        DataAvtovokzal getData = new DataAvtovokzal();

        // Открываем страницу сайта автовокзала
        System.out.println("Open website");
        driver.get(getData.getUrl);

        // Вводим пункт отправления
        send(driver.findElement(inputCityFrom), getData.cityFromVolgograd);
        waitAndBtnDownEnter(driver.findElement(inputCityFrom), driver.findElement(cityVolgograd));

        // Вводим пункт назначения
        send(driver.findElement(inputCityTo), getData.cityToMoscow);
        waitAndBtnDownEnter(driver.findElement(inputCityTo), driver.findElement(cityMoskow));

        // Выбираем дату отправления
       // driver.findElement(dateFrom).click();
      //  driver.findElement(By.linkText(getData.dateFrom)).click();

        // Найти рейсы
        driver.findElement(btnFind).click();

        // Проверка на рейсы Москвы
        String getError01 = driver.findElement(getErrorText).getText();

        Assertions.assertEquals("Расписание автобуса Волгоград - Москва г.", getError01);

        // Распарсить все отправления на выбранную дату

        // Нажать на кнопку следующей даты
        driver.findElement(btnNext).click();

        // Распарсить все отправления на следующую дату

        // Проверка на рейсы Москвы
        String getError02 = driver.findElement(getErrorText).getText();

        Assertions.assertEquals("Расписание автобуса Волгоград - Москва г.", getError02);
    }

    protected void send(WebElement field, String city) {
        System.out.println("send locator");
        field.click();
        field.sendKeys(city);
    }

    protected void waitAndBtnDownEnter(WebElement field, WebElement locator) {
        wait.until(ExpectedConditions.visibilityOf(locator));
        field.sendKeys(Keys.DOWN);
        field.sendKeys(Keys.RETURN);
    }
}
