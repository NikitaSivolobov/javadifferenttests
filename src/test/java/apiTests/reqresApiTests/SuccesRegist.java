package apiTests.reqresApiTests;

public class SuccesRegist {
    public Integer id;
    public String token;

    public SuccesRegist(Integer id, String token) {
        this.id = id;
        this.token = token;
    }

    public SuccesRegist() {
    }

    public Integer getId() {
        return id;
    }

    public String getToken() {
        return token;
    }
}
