package apiTests.reqresApiTests;


import apiTests.reqresApiTests.specification.Specifications;
import io.qameta.allure.Description;
import io.qameta.allure.Owner;
import org.junit.jupiter.api.*;

import java.util.List;
import java.util.stream.Collectors;

import static io.restassured.RestAssured.given;

@Tag("API")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ApiReqresTests {

    private final static String URL = "https://reqres.in/";

    @Test
    @Order(1)
    @Owner("Nikita Sivolobov")
    @Description("Api проверка метода Get")
    public void checkUserNameTest() {
        Specifications.installSpecification(Specifications.requestSpec(URL), Specifications.responseSpecOK200());
        List<Data> data = given()
                .when()
                .get("api/users?page=2")
                .then().log().all()
                .extract().body().jsonPath().getList("data", Data.class);
        Assertions.assertTrue(data.stream().allMatch(x -> x.getEmail().endsWith("@reqres.in")));

        List<String> avatars = data.stream().map(Data::getAvatar).collect(Collectors.toList());
        List<String> ids = data.stream().map(x -> x.getId().toString()).collect(Collectors.toList());
        for (int i = 0; i < avatars.size(); i++) {
            Assertions.assertTrue(avatars.get(i).contains(ids.get(i)));
        }
    }

    @Test
    @Order(2)
    @Owner("Nikita Sivolobov")
    @Description("Api проверка метода Post")
    public void successRegTest() {
        Specifications.installSpecification(Specifications.requestSpec(URL), Specifications.responseSpecOK200());
        Integer id = 4;
        String token = "QpwL5tke4Pnpja7X4";
        Regist user = new Regist("eve.holt@reqres.in", "pistol");
        SuccesRegist succesRegist = given()
                .body(user)
                .when()
                .post("api/register")
                .then().log().all()
                .extract().as(SuccesRegist.class);
        Assertions.assertNotNull(succesRegist.getId());
        Assertions.assertNotNull(succesRegist.getToken());

        Assertions.assertEquals(id, succesRegist.getId());
        Assertions.assertEquals(token, succesRegist.getToken());
    }

    @Test
    @Order(3)
    @Owner("Nikita Sivolobov")
    @Description("Api проверка метода Delete")
    public void deleteUserTest() {
        Specifications.installSpecification(Specifications.requestSpec(URL), Specifications.responseSpecUniq(204));
        given()
                .when()
                .delete("api/users/2")
                .then().log().all();
    }
}

