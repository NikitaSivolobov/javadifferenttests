package apiTests.spaceXApiTests;

import apiTests.reqresApiTests.specification.Specifications;
import com.jayway.jsonpath.JsonPath;
import io.qameta.allure.Description;
import io.qameta.allure.Owner;
import io.restassured.RestAssured;
import okhttp3.ResponseBody;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.GET;

import java.io.IOException;
import java.util.Objects;

import static org.hamcrest.Matchers.equalTo;

@Tag("API")
public class SpaceXApiTests {

    private final static String URL_SPACE = "https://api.spacexdata.com/v4/";

    @Test
    @Owner("Nikita Sivolobov")
    @Description("Api проверка метода Get")
    @DisplayName("RestAssured testing Get method with check field")
    public void getCompanyRestAssuredTest(){
        Specifications.installSpecification(Specifications.requestSpec(URL_SPACE), Specifications.responseSpecOK200());
        RestAssured
                .get("https://api.spacexdata.com/v4/company")
                .then().log().body()
                .assertThat()
                .body("cto_propulsion", equalTo("Tom Mueller"));
    }


    @Test
    @Owner("Nikita Sivolobov")
    @Description("Api проверка метода Get")
    @DisplayName("Retrofit testing Get method with check field")
    public void getCompanyRetrofitTest() throws IOException {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL_SPACE)
                .build();

        SpaceXService service = retrofit.create(SpaceXService.class);
        Call<ResponseBody> companyInfo = service.getCompanyInfo();

        Response<ResponseBody> response = companyInfo.execute();
        String responseBody = Objects.requireNonNull(response.body()).string();

        int valuation = JsonPath.read(responseBody, "employees");
        Assertions.assertEquals(valuation, 9500);
    }

    interface SpaceXService {
        @GET("company/")
        Call<ResponseBody> getCompanyInfo();
    }
}
