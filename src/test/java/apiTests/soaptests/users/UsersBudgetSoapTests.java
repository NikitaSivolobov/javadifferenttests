package apiTests.soaptests.users;

import io.qameta.allure.Description;
import io.qameta.allure.Owner;
import io.restassured.RestAssured;
import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;


import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.*;

import java.io.File;
import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;

import static io.restassured.RestAssured.given;
import static java.nio.charset.StandardCharsets.*;


@Tag("API")
public class UsersBudgetSoapTests {

    private final static String URL_USERS = "http://users.bugred.ru";

    @Test
    @Owner("Nikita Sivolobov")
    @Description("проверка SOAP")
    @DisplayName("RestAssured testing SOAP method with check field")
    public void getUsersRestAssuredSOAPTest() throws Exception {
        FileInputStream fileInputStream = new FileInputStream(new File(".\\src\\test\\java\\apiTests\\soaptests\\users\\getUser.xml"));
        RestAssured.baseURI=URL_USERS;
        Response response=given()
                .header("Content-Type", "text/xml")
                .and()
                .body(IOUtils.toString(fileInputStream, UTF_8))
                .when()
                .post("/tasks/soap/WrapperSoapServer.php")
                .then()
                .statusCode(200)
                .and()
                .log().all()
                .extract().response();
        XmlPath xmlpath = new XmlPath(response.asString());//Converting string into xml path to assert
        String name=xmlpath.getString("email");
        System.out.println("name returned is: " +  name);
        String expectedName = "test@test.testtesthttp://users.bugred.ru//tmp/default_avatar.jpgee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff0null0nullnullnullnullnullnullnullnullnullnullnullnullnull";
        Assertions.assertEquals(expectedName, name);
    }
}
