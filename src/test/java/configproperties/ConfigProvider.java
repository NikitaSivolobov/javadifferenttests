package configproperties;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public interface ConfigProvider {
    Config config = readConfig();

    static Config readConfig(){
        return ConfigFactory.systemProperties().hasPath("testProfile")
                ? ConfigFactory.load(ConfigFactory.systemProperties().getString("testProfile"))
                : ConfigFactory.load("application.conf");
    }

    String URL_ASTON = readConfig().getString("urlAston");
//    String URL_ASTON = readConfig().getString("urlParams.main.aston");
    String URL_YA = readConfig().getString("urlYa");
//    String URL_YA = readConfig().getString("urlParams.main.yandex");
}
